<!DOCTYPE html>
<html lang="ml-IN">
	<head>
		<meta charset="UTF-8">
		<meta name="description" content="Malayalam unicode characters and their characteristics">
		<meta name="keywords" content="Malayalam, Phonetics, IPA, Grammar">
		<meta name="author" content="Ajith R">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<style>
			@import url('https://smc.org.in/fonts/meera.css');
			html { font-family: 'Meera', sans-serif; }
			table{table-layout:fixed;}
			td, th {
				width:98px;
				height: 25px;
				border: 1px solid #ccc;
				text-align: center;
			}
			td {
				position:relative;
				background: #ADD8E6;
			}
			th {
				position: sticky;
			}
			th:first-child {
				z-index: 50;
				left: 1px;
				background: #3CB371;
			}
			th.f {
				background: #DB7093;
				z-index: 100;
				top: 1px;
			}
			th.s {
				background:#DB7093;
				z-index: 100;
				top: 30px;
			}
			td.c {
				background: #FFA07A;
			}
			td.djf {
				font-family: Dejavu Sans; font-style: normal; font-variant: normal;
			}
			span {
				display:none;
				position:absolute; 
				z-index:10;
				border:1px;
				background-color:#D3D3D3;
				border-style:solid;
				border-width:1px;
				border-radius: 5px;
				padding:3px;
				color:black; 
				top:0px; 
				left:50px;
				width:500px;
			}
			body {
				padding: 1rem;
			}
			th:hover span{
				display:block;
			}
			td:hover span{
				display:block;
			}
			select {
				border: none;
				border-radius: 5px;
				background-color: #DB7093;
			}
		</style>
		<script>
			function ocscat() {
			var input, filter, table, tr, td, i;
			input = document.getElementById("scat");
			filter = input.options[input.selectedIndex].text;
			table = document.getElementById("details");
			tr = table.getElementsByTagName("tr");
			document.getElementById("demo") = "test";
			for (i = 0; i < tr.length; i++) {
				td = tr[i].getElementsByTagName("td")[3];

				if (td) {
					if (td.innerHTML.includes(filter)) {
						tr[i].style.display = "";
					} else {
					tr[i].style.display = "none";
					}

				}       
				}
			}
		</script>

	</head>

<body>

<h1> മലയാളലിപികളും അവയുടെ ഗുണങ്ങളും - ഒരു പട്ടിക <a href ="about.html"  style="color: grey;"> (വിശദാംശങ്ങൾ ഇവിടെ) </a></h1>


<table id="details">

<tr>
<th class="f">Character</th>

<th class="f">Name</th>

<th class="f">Unicode</th>

<th class="f">IPA</th>

<th class="f">	
<select name="scat" id="scat" onchange="ocscat()" '>
<option value="all">Category</option>
<option value="vwll">സ്വരം</option>
<option value="cnst">വ്യഞ്ജനം</option>
<option value="vwlm">സ്വരചിഹ്നം</option>
<option value="smbl">പ്രതീകം</option>
<option value="chll">ചില്ല്</option>
<option value="numi">പൂർണ്ണസംഖ്യ</option>
<option value="numf">ഭിന്നസംഖ്യ</option>
<option value="othr">ഉച്ചാരണനിർദ്ദേശങ്ങള്‍</option>
</select>
</th>

<th class="f">
<select name="stricture" id="stricture">
<option value="all">Stricture</option>
<option value="vl">അസ്പൃഷ്ടം</option>
<option value="stp">സ്പൃഷ്ടം</option>
<option value="ap">ഈഷത്സ്പൃഷ്ടം</option>
<option value="fr">നേമസ്പൃഷ്ടം</option>
<option value="na">NA</option>
</select>
</th>

<th class="f">
<select name="poa" id="poa">
<option value="all">Position</option>
<option value="sp">മൃദുതാലവ്യം</option>
<option value="hp">താലവ്യം</option>
<option value="rf">മൂർദ്ധന്യം</option>
<option value="al">വർത്സ്യം</option>
<option value="dl">ദന്ത്യം</option>
<option value="lb">ഓഷ്ഠ്യം</option>
<option value="da">ദന്ത്യവർത്സ്യം</option>
<option value="ld">ദന്തോഷ്ഠ്യം</option>
<option value="vl">കണ്ഠ്യം</option>
<option value="na">NA</option>
</select>
</th>

<th class="f">
<select name="moa" id="moa">
<option value="all">Mechanism</option>
<option value="stp">സ്പർശം</option>
<option value="ap">പ്രവാഹി</option>
<option value="tr">സ്ഫുരിതം</option>
<option value="lt">പാർശ്വികം</option>
<option value="fr">ഘർഷം</option>
<option value="na">NA</option>
</select>
</th>

<th class="f">
<select name="voice" id="voice">
<option value="all">Voice</option>
<option value="vd">നാദി</option>
<option value="nv">ശ്വാസി</option>
<option value="na">NA</option>
</select>
</th>

<th class="f">
<select name="asp" id="asp">
<option value="all">Aspiration</option>
<option value="nasp">അല്പപ്രാണം</option>
<option value="asp">മഹാപ്രാണം</option>
<option value="na">NA</option>
</select>
</th>

<th class="f">
<select name="nasality" id="nasality">
<option value="all">Route</option>
<option value="oral">നിരനുനാസികം</option>
<option value="nasal">അനുനാസികം</option>
<option value="na">NA</option>
</select>
</th>


<th class="f">
<select name="mgcat" id="mgcat">
<option value="all">Mal Grammar Categroy</option>
<option value="short">ഹ്രസ്വം</option>
<option value="long">ദീർഘം</option>
<option value="diph">ദ്വിസ്വരം</option>
<option value="vk">ഖരം</option>
<option value="vkh">അതിഖരം</option>
<option value="vm">മൃദു</option>
<option value="vgh">ഘോഷം</option>
<option value="vn">അനുനാസികം</option>
<option value="vm">മധ്യമം</option>
<option value="vf">ഊഷ്മാക്കൾ</option>
<option value="diph">ഘോഷി</option>
<option value="na">NA</option>
</select>
</th>

<th class="f">
<select name="mclass" id="mclass">
<option value="all">Mal Letter Class</option>
<option value="kv">കവർഗ്ഗം</option>
<option value="cv">ചവർഗ്ഗം</option>
<option value="ttv">ടവർഗ്ഗം</option>
<option value="tv">ഺവർഗ്ഗം</option>
<option value="thv">തവർഗ്ഗം</option>
<option value="pv">പവർഗ്ഗം</option>
<option value="na">NA</option>
</select>
</th>

<th class="f">
<select name="hard" id="hard">
<option value="all">Hardness</option>
<option value="hrd">ദൃഢം</option>
<option value="sft">ശിഥിലം</option>
<option value="na">NA</option>
</select>
</th>

<th class="f">
<select name="usage" id="usage">
<option value="all">Usage</option>
<option value="cmn">സാധാരണം</option>
<option value="rare">വിരളം</option>
</select>
</th>

<th class="f">
<select name="ugc" id="ugc">
<option value="all">Uni Gen Category</option>
<option value="lo">Lo</option>
<option value="mc">Mc</option>
<option value="mn">Mn</option>
<option value="so">So</option>
<option value="nd">Nd</option>
<option value="no">No</option>
</select>
</th>

<th class="f">Sort Order</th>
</tr>

<tr>
<th class="s">ലിപി</th>
<th class="s">പേര്</th>
<th class="s">യൂഩികോഡ്</th>
<th class="s">ഐ പി എ</th>
<th class="s">വിഭാഗം</th>
<th class="s">അനുപ്രദാനം</th>
<th class="s">സ്ഥാനം</th>
<th class="s">രീതി</th>
<th class="s">കമ്പനരൂപം</th>
<th class="s">സംസർഗം </th>
<th class="s">നാസിക്യത</th>
<th class="s">ഇനം</th>
<th class="s">വര്‍ഗം</th>
<th class="s">കാഠിന്യം</th>
<th class="s">പ്രയോഗം</th>
<th class="s">യൂഩി വിഭാഗം</th>
<th class="s">ക്രമം</th>
</tr>